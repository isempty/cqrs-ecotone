<?php

declare(strict_types=1);

namespace App\Domain\Product;

final class RegisterProductCommand
{
    public function __construct(
        public readonly int $productId,
        public readonly Cost $cost,
    ) {
    }
}
