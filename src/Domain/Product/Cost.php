<?php

declare(strict_types=1);

namespace App\Domain\Product;

final class Cost
{
    public function __construct(
        public readonly int $amount
    ) {
        if ($this->amount <= 0) {
            throw new \InvalidArgumentException(
                message: sprintf('The cost cannot be negative or zero, %s given.', $this->amount)
            );
        }
    }

    public function __toString(): string
    {
        return (string) $this->amount;
    }
}
