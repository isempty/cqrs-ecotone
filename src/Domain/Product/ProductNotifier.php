<?php

declare(strict_types=1);

namespace App\Domain\Product;

use Ecotone\Modelling\Attribute\EventHandler;

final class ProductNotifier
{
    #[EventHandler]
    public function notifyAbout(ProductWasRegistredEvent $event): void
    {
        // logic
        echo sprintf('Product with id %s was regisstred!%s', $event->productId, PHP_EOL);
    }
}
