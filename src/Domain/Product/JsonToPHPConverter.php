<?php

declare(strict_types=1);

namespace App\Domain\Product;

use Ecotone\Messaging\Attribute\MediaTypeConverter;
use Ecotone\Messaging\Conversion\Converter;
use Ecotone\Messaging\Conversion\MediaType;
use Ecotone\Messaging\Handler\TypeDescriptor;

#[MediaTypeConverter]
final class JsonToPHPConverter implements Converter
{
    public function convert(
        $source,
        TypeDescriptor $sourceType,
        MediaType $sourceMediaType,
        TypeDescriptor $targetType,
        MediaType $targetMediaType
    ): mixed {
        $data = json_decode($source, true, flags: JSON_THROW_ON_ERROR);

        return match ($targetType->getTypeHint()) {
            RegisterProductCommand::class => RegisterProductCommand::fromArray($data),
            GetProductPriceQuery::class => GetProductPriceQuery::fromArray($data),
            default => throw new \InvalidArgumentException('Unknown conversion type')
        };
    }

    public function matches(
        TypeDescriptor $sourceType,
        MediaType $sourceMediaType,
        TypeDescriptor $targetType,
        MediaType $targetMediaType
    ): bool {
        return $sourceMediaType->isCompatibleWith(MediaType::createApplicationJson())
            && $targetMediaType->isCompatibleWith(MediaType::createApplicationXPHP());
    }
}
