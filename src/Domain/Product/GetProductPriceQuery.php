<?php

declare(strict_types=1);

namespace App\Domain\Product;

final class GetProductPriceQuery
{
    public function __construct(
        public readonly int $productId,
    ) {
    }
}
