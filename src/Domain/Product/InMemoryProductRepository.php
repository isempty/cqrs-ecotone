<?php

declare(strict_types=1);

namespace App\Domain\Product;

use Ecotone\Modelling\Attribute\Repository;
use Ecotone\Modelling\StandardRepository;

#[Repository]
final class InMemoryProductRepository implements StandardRepository
{
    private array $products;

    public function canHandle(string $aggregateClassName): bool
    {
        return $aggregateClassName === Product::class;
    }

    public function findBy(string $aggregateClassName, array $identifiers): ?object
    {
        if (! array_key_exists($identifiers['productId'], $this->products)) {
            return null;
        }

        return $this->products[$identifiers['productId']];
    }

    public function save(array $identifiers, object $aggregate, array $metadata, ?int $versionBeforeHandling): void
    {
        $this->products[$identifiers['productId']] = $aggregate;
    }
}
