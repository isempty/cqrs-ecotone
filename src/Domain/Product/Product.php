<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Infrastructure\AddUserId\AddUserId;
use App\Infrastructure\RequireAdministrator\RequireAdministrator;
use Ecotone\Messaging\Attribute\Parameter\Headers;
use Ecotone\Messaging\Attribute\Parameter\Payload;
use Ecotone\Modelling\Attribute\Aggregate;
use Ecotone\Modelling\Attribute\AggregateIdentifier;
use Ecotone\Modelling\Attribute\CommandHandler;
use Ecotone\Modelling\Attribute\QueryHandler;
use Ecotone\Modelling\WithAggregateEvents;

#[Aggregate]
#[AddUserId]
final class Product
{
    use WithAggregateEvents;

    public function __construct(
        #[AggregateIdentifier]
        public readonly int $productId,
        private Cost $cost,
        public readonly int $userId
    ) {
        $this->recordThat(new ProductWasRegistredEvent($this->productId));
    }

    #[CommandHandler('product.register')]
    #[RequireAdministrator]
    public static function register(
        #[Payload]
        RegisterProductCommand $command,
        #[Headers]
        array $metaData,
    ): static {
        return new static(
            productId: $command->productId,
            cost: $command->cost,
            userId: $metaData['userId']
        );
    }

    #[CommandHandler('product.changePrice')]
    #[RequireAdministrator]
    public function changePrice(ChangePriceCommand $command): void
    {
        $this->cost = $command->cost;
    }

    #[QueryHandler('product.getCost')]
    public function getCost(GetProductPriceQuery $query): Cost
    {
        return $this->cost;
    }
}
