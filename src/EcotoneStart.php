<?php


namespace App;

use Ecotone\Modelling\CommandBus;
use Ecotone\Modelling\QueryBus;

class EcotoneStart
{
    public function __construct(
        private CommandBus $commandBus,
        private QueryBus $queryBus
    ) {
    }

    public function run() : void
    {
        $this->commandBus->sendWithRouting(
            'product.register',
            json_encode(["productId" => 1, "cost" => 100]),
            "application/json",
            ['userId' => 1]
        );

        $this->commandBus->sendWithRouting(
            'product.changePrice',
            json_encode(["productId" => 1, "cost" => 125]),
            "application/json",
            ['userId' => 1]
        );

        echo $this->queryBus->sendWithRouting(
            'product.getCost',
            \json_encode(["productId" => 1]),
            "application/json"
        );
    }
}
