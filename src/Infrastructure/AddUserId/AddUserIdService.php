<?php

declare(strict_types=1);

namespace App\Infrastructure\AddUserId;

use Ecotone\Messaging\Attribute\Interceptor\Before;

final class AddUserIdService
{
    #[Before(
        precedence: 0,
        pointcut: AddUserId::class,
        changeHeaders: true
    )]
    public function add(): array
    {
        return ['userId' => 1];
    }
}
