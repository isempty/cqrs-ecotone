<?php

declare(strict_types=1);

namespace App\Infrastructure\CostConverter;

use App\Domain\Product\Cost;
use Ecotone\Messaging\Attribute\Converter;

final class CostConverter
{
    #[Converter]
    public function convertFrom(Cost $cost): int
    {
        return $cost->amount;
    }

    #[Converter]
    public function convertTo(int $amount): Cost
    {
        return new Cost($amount);
    }
}
