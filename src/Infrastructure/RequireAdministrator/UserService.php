<?php

declare(strict_types=1);

namespace App\Infrastructure\RequireAdministrator;

use Ecotone\Messaging\Attribute\Interceptor\Before;
use Ecotone\Messaging\Attribute\Parameter\Header;

final class UserService
{
    #[Before(
        precedence: 1,
        pointcut: RequireAdministrator::class
    )]
    public function isAdmin(#[Header('userId')] ?string $userId): void
    {
        if ($userId != 1) {
            throw new \InvalidArgumentException(
                message: 'You need to be administrator in order to register new product'
            );
        }
    }
}
